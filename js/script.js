"use strict";

// let timerId = setTimeout(sayHello, 3000);
// clearTimeout(timerId);
// let timerId = setInterval(sayHello, 3000);
// clearTimeout(timerId);

// function sayHello () {
//     console.log('Hello World');
// }

// let timerId = setTimeout(function log() {
//     console.log('Hello');
//     setTimeout(log, 4000);
// });

let btn = document.querySelector('.btn'),
    elem = document.querySelector('.red_box-blue');

function myAnimation (){
    let pos = 0;

    let id = setInterval(frame, 10);
    function frame (){
        if (pos == 315) {
            clearInterval(id);
        } else {
            pos++;
            elem.style.top = pos + 'px';
            elem.style.left = pos + 'px';

        }
    }
}

btn.addEventListener('click', myAnimation);

let btnBlock = document.querySelector('.grey_box'),
    btns = document.getElementsByTagName('.grey_box-yellow');

btnBlock.addEventListener('click', function(event) {
    if (event.target && event.target.matches('button.first')) {
        console.log('hi');
    }    
});